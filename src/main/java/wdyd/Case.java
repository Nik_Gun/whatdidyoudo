package wdyd;

public class Case {
    public int id, isDone;
    String event_date = "", value = "";

    public Case(int id, String event_date, String value) {
        this.event_date = event_date;
        this.value = value;
        this.id = id;
    }

    public Case(int id, String event_date, String value, int isDone) {
        this.event_date = event_date;
        this.value = value;
        this.isDone=isDone;
        this.id = id;
    }
}
