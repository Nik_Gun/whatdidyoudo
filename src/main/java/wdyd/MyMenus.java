package wdyd;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;

public class MyMenus {
    DataBase db;

    MyMenus() {
        db = DataBase.getInstanse();
    }

    public ReplyKeyboardMarkup mainMenu(Long id) {
        TgKeyBoardBuilder keyBoardBuilder = new TgKeyBoardBuilder();
        db.setMenuState(id, -1);
        keyBoardBuilder.addTextButton("Настройки", 1);
        keyBoardBuilder.addTextButton("Пауза", 1);
        keyBoardBuilder.addTextButton("Список дел", 2);
        keyBoardBuilder.addTextButton("Статистика дел", 3);
        keyBoardBuilder.addTextButton("Статистика задач", 3);
        return keyBoardBuilder.getKeyboardMarkup();
    }
}
