package wdyd;

import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class NotifyTimer extends Thread {
    MyAbilityTelegramBot myAbilityTelegramBot;
    DataBase db;
    MyMenus myMenus;

    public NotifyTimer(MyAbilityTelegramBot bot) {
        myAbilityTelegramBot = bot;
        db = DataBase.getInstanse();
        myMenus = new MyMenus();
        this.start();
    }

    String lastH = "-1";

    public void run() {
        int index = 0;//[0] - часы [1] - минуты
        lastH = getCurrentTime().split(":")[index];
        while (true) {
            if (!lastH.equals(getCurrentTime().split(":")[index])) {
                lastH = getCurrentTime().split(":")[index];
                ArrayList<Long> users = db.getUsersID();
                for (Long user : users) {
                    if (isActive(user)) {

                        db.setPause(user, db.getPeriod(user) - 1);
                        if (db.isStart(user, getCurrentTime())) {
                            ArrayList<Case> cases = db.getCasesToday(user, myAbilityTelegramBot.getCurrentTime());
                            if (cases.size() == 0) {
                                myAbilityTelegramBot.sendTextMessageWithKeyBoard(user, "У вас нет запланированных задач на сегодня", myMenus.mainMenu(user));
                            } else {
                                StringBuilder stringBuilder = new StringBuilder();
                                stringBuilder.append("Вам на сегодня\n\n");
                                for (Case c : cases) {

                                    //  stringBuilder.append("Задача на сегодня(" + c.event_date + ")");
                                    stringBuilder.append(c.value + "\n\n");
                                    // TgKeyBoardBuilder kb = new TgKeyBoardBuilder();
                                    //   kb.addInlineButton("Сделано", "done:" + c.id, 1);

                                }
                                myAbilityTelegramBot.sendTextMessage(user, stringBuilder.toString()/*, kb.getInlineKeyboardMarkup()*/);
                            }
                        } else if (db.isEnd(user, getCurrentTime())) {
                            ArrayList<Case> cases = db.getCasesTomorrow(user, myAbilityTelegramBot.getModifyTime(myAbilityTelegramBot.getCurrentTime(), 1));
                            if (cases.size() == 0) {
                                myAbilityTelegramBot.sendTextMessageWithKeyBoard(user, "У вас нет запланированных задач на завтра", myMenus.mainMenu(user));
                            } else {
                                StringBuilder stringBuilder = new StringBuilder();
                                stringBuilder.append("Вам на завтра\n\n");
                                for (Case c : cases) {

                                    ///stringBuilder.append("Задача на завтра(" + c.event_date + ")");
                                    stringBuilder.append(c.value + "\n\n");
                                    // TgKeyBoardBuilder kb = new TgKeyBoardBuilder();
                                    //   kb.addInlineButton("Сделано", "done:" + c.id, 1);

                                }
                                myAbilityTelegramBot.sendTextMessage(user, stringBuilder.toString()/*, kb.getInlineKeyboardMarkup()*/);
                            }
                        } else {
                            myAbilityTelegramBot.sendTextMessageWithKeyBoard(user, "Что сделал?", myMenus.mainMenu(user));
                        }
                    }
                }
            }
        }
    }

    private boolean isActive(Long user) {
        if (!db.isTimeRange(user, getCurrentTime())) {
            return false;
        } else {
            if (db.isPause(user)) {
                return false;
            }
        }
        return true;
    }

    String getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return getModifyTime(sdf.format(cal.getTime()), 60 * 3);
    }

    String getModifyTime(String time, int m) {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date dtime = null;
        try {
            dtime = sdf.parse(time);
            dtime = DateUtils.addMinutes(dtime, m);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(dtime.getTime());
    }
}
