package wdyd;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExcelController {
    DataBase db;
    XSSFWorkbook myExcelBook = null;
    XSSFSheet myExcelSheet;
    String file = "";

    ExcelController(String f) {
        file = f;

        // new CreateExcelDemo();
        openExcel(f);

    }

    void openExcel(String f) {
        try {
            myExcelBook = new XSSFWorkbook(new FileInputStream(f));
            myExcelSheet = myExcelBook.getSheet("template");
        } catch (IOException e) {
            e.printStackTrace();
        }

        //XSSFRow row = myExcelSheet.getRow(0);
        //  System.out.println(row.getCell(1));
    }

    void mergedRegion(int firstRow, int lastRow, int firstCol, int lastCol) {
        myExcelSheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
    }

    String getCell(int r, int c) {
        Row row = myExcelSheet.getRow(r);
        Cell cell = row.getCell(c);
        return cell.getStringCellValue();
    }

    void setTextToCell(String value, int r, int c) {
        Row row = myExcelSheet.getRow(r);
        if (row == null)
            row = myExcelSheet.createRow(r);
        Cell cell = row.getCell(c, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
        CellStyle cellStyle = cell.getCellStyle();
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cell.setCellStyle(cellStyle);


        cell.setCellValue(value);
    }

    void closeBook() {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            myExcelBook.write(fos);
            fos.close();
            myExcelBook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
