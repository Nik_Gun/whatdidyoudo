package wdyd;

import org.apache.commons.lang3.time.DateUtils;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.awt.image.Kernel;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MyAbilityTelegramBot extends AbilityBot {
    DataBase db;
    MyMenus myMenus;

    protected MyAbilityTelegramBot(String botToken, String botUsername, DefaultBotOptions botOptions) {
        super(botToken, botUsername, botOptions);
        db = DataBase.getInstanse();
        myMenus = new MyMenus();
        new NotifyTimer(this);
    }

    void sendTextMessage(Long to, String text) {
        try {
            SendMessage snd = new SendMessage();
            snd.setChatId(to);
            snd.setText(text);
            execute(snd);
        } catch (TelegramApiException e) {

        }
    }

    public void sendTextMessageWithInlineKeyBoard(Long to, String text, InlineKeyboardMarkup inlineKeyboardMarkup) {
        try {
            SendMessage snd = new SendMessage();
            snd.setChatId(to);
            snd.setText(text);
            snd.setReplyMarkup(inlineKeyboardMarkup);
            execute(snd);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }


    synchronized void sendMarkdownV2Message(Long to, String text) {
        try {
            SendMessage snd = new SendMessage();
            snd.setChatId(to);
            snd.setText(text);
            snd.setParseMode("markdown");
            execute(snd);
        } catch (TelegramApiException e) {

        }
    }

    void sendMessageWithDocument(Long to,int template) {
        try {
            new ExcelCreateTable(to,template);
            SendDocument sendDocument = new SendDocument();
            File f = new File("excel/tables/" + to + ".xlsx");
            sendDocument.setDocument(f);
            sendDocument.setCaption("Вот список того что вы делали");
            sendDocument.setChatId(to);
            execute(sendDocument);
            f.delete();
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    void sendTextMessageWithKeyBoard(Long to, String text, ReplyKeyboardMarkup keyboardMarkup) {
        try {
            SendMessage snd = new SendMessage();
            snd.setChatId(to);
            snd.setText(text);
            snd.setReplyMarkup(keyboardMarkup);
            execute(snd);
        } catch (TelegramApiException e) {

        }
    }

    @Override
    public int creatorId() {
        return 448596921;
    }

    /**
     * Menu Description
     * -1 - меню
     * 1 - настройки
     * 2 - пауза
     */
    @Override
    public void onUpdatesReceived(List<Update> updates) {
        if (updates.size() >= 1) {

            Update update = updates.get(updates.size() - 1);
            Message message = update.getMessage();

            TgKeyBoardBuilder keyBoardBuilder = new TgKeyBoardBuilder();
            if (message != null) {
                Chat chat = message.getChat();
                String text = message.getText();
                System.out.println(getCurrentTime() + ":\t" + text);
                Long id = chat.getId();
                if (!db.isUserExist(id)) {
                    db.addUser(id, chat.getUserName());
                    sendTextMessageWithKeyBoard(id, "Добро пожаловать в бот, зайди в настройки и настрой уведомления", myMenus.mainMenu(id));
                } else {
                    if (text.equals("Выход")) {
                        db.removeUser(id);
                        keyBoardBuilder.addTextButton("Начать", 1);
                        sendTextMessageWithKeyBoard(id, "Вы вышли!", keyBoardBuilder.getKeyboardMarkup());
                    } else {
                        switch (db.getMenu(id)) {
                            case -1:
                                if (text.equals("Настройки")) {
                                    db.setMenuState(id, 1);
                                    keyBoardBuilder.addTextButton("Время работы", 1);
                                    keyBoardBuilder.addTextButton("Период напоминаний", 1);
                                    keyBoardBuilder.addTextButton("Отмена", 2);
                                    sendTextMessageWithKeyBoard(id, "Меню настроек", keyBoardBuilder.getKeyboardMarkup());
                                } else if (text.equals("Пауза")) {
                                    db.setMenuState(id, 2);
                                    keyBoardBuilder.addTextButton("1", 1);
                                    keyBoardBuilder.addTextButton("2", 1);
                                    keyBoardBuilder.addTextButton("3", 1);
                                    keyBoardBuilder.addTextButton("4", 1);
                                    keyBoardBuilder.addTextButton("24", 2);
                                    keyBoardBuilder.addTextButton("Пауза", 3);
                                    keyBoardBuilder.addTextButton("Возобновить", 3);
                                    keyBoardBuilder.addTextButton("Отмена", 4);
                                    sendTextMessageWithKeyBoard(id, "Укажите время на которое хотите установить паузу (в часах)", keyBoardBuilder.getKeyboardMarkup());
                                } else if (text.equals("Список дел")) {
                                    ArrayList<Case> cases = db.getCases(id);
                                    if (cases.size() == 0) {
                                        sendTextMessageWithKeyBoard(id, "У вас нет запланированных задач", myMenus.mainMenu(id));
                                    } else {
                                        for (Case c : cases) {
                                            StringBuilder stringBuilder = new StringBuilder();
                                            stringBuilder.append("Задача на " + c.event_date);
                                            stringBuilder.append("\n\n" + c.value);
                                            TgKeyBoardBuilder kb = new TgKeyBoardBuilder();
                                            kb.addInlineButton("Сделано", "done:" + c.id, 1);
                                            sendTextMessageWithInlineKeyBoard(id, stringBuilder.toString(), kb.getInlineKeyboardMarkup());
                                        }
                                    }
                                } else if (text.equals("Статистика дел")) {
                                    //new ExcelCreateTable(id,1);
                                    sendMessageWithDocument(id,1);
                                } else if (text.equals("Статистика задач")) {
                                    //new ExcelCreateTable(id,2);/
                                    sendMessageWithDocument(id,2);
                                } else {
                                    int isCase = checkIsCase(text);
                                    if (isCase > 0) {
                                        String time = "";
                                        switch (isCase) {
                                            case 1://сегодня
                                                time = getCurrentTime();
                                                break;
                                            case 2://завтра
                                                time = getModifyTime(getCurrentTime(), 1);
                                                break;
                                            case 3://Послезавтра
                                                time = getModifyTime(getCurrentTime(), 2);
                                                break;
                                            case 4://Следующий неделе
                                                time = getModifyTime(getCurrentTime(), 7);
                                                break;
                                        }
                                        db.addCase(id, time, text.substring(isCase));
                                        sendTextMessageWithKeyBoard(id, "Запланиравоно на " + time.split(" ")[0], myMenus.mainMenu(id));
                                    } else {
                                        db.addEvent(id, getCurrentTime(), text);
                                    }
                                }
                                break;
                            case 1:
                                if (text.equals("Отмена")) {
                                    sendTextMessageWithKeyBoard(id, "Добро пожаловать в бот, зайди в настройки и настрой уведомления", myMenus.mainMenu(id));
                                } else if (text.equals("Время работы")) {
                                    db.setMenuState(id, 11);
                                    keyBoardBuilder.addTextButton("Отмена", 1);
                                    sendTextMessageWithKeyBoard(id, "Укажите время работы бота например напишите - \"10-22\" (стоит по умолчанию)" + "\nСейчас:" + db.getTime(id), keyBoardBuilder.getKeyboardMarkup());
                                } else if (text.equals("Период напоминаний")) {
                                    db.setMenuState(id, 12);
                                    keyBoardBuilder.addTextButton("Отмена", 1);
                                    sendTextMessageWithKeyBoard(id, "Укажите в целых часах период напоминаний(по умолчанию стоит 1 час)", keyBoardBuilder.getKeyboardMarkup());
                                }
                                break;
                            case 11:
                                if (text.equals("Отмена")) {
                                    sendTextMessageWithKeyBoard(id, "Зайди в настройки чтобы настроить уведомления", myMenus.mainMenu(id));
                                } else {
                                    boolean isFormat = true;
                                    text.replace(" ", "");
                                    if (text.split("-").length == 2) {
                                        try {
                                            String s = "";
                                            String e = "";

                                            int start = Integer.parseInt(text.split("-")[0]);
                                            int end = Integer.parseInt(text.split("-")[1]);
                                            if (start < 10) {
                                                s += "0";
                                            }
                                            if (end < 10) {
                                                e += "0";
                                            }
                                            s += start + ":00";
                                            e += end + ":00";
                                            db.setTime(id, s, e);
                                        } catch (NumberFormatException nfe) {
                                            isFormat = false;
                                        }
                                    } else {
                                        isFormat = false;
                                    }
                                    if (!isFormat) {
                                        keyBoardBuilder.addTextButton("Отмена", 1);
                                        sendTextMessageWithKeyBoard(id, "Укажите время работы бота например напишите - \"9-22\" (стоит по умолчанию)" + "\nСейчас:" + db.getTime(id), keyBoardBuilder.getKeyboardMarkup());
                                    } else {
                                        sendTextMessageWithKeyBoard(id, "Зайди в настройки чтобы настроить уведомления", myMenus.mainMenu(id));
                                    }
                                }
                                break;
                            case 12:
                                if (text.equals("Отмена")) {
                                    sendTextMessageWithKeyBoard(id, "Зайди в настройки чтобы настроить уведомления", myMenus.mainMenu(id));
                                } else {
                                    boolean isFormat = true;
                                    text.replace(" ", "");
                                    try {

                                        int per = Integer.parseInt(text);
                                        db.setPeriod(id, per);
                                    } catch (NumberFormatException nfe) {
                                        isFormat = false;
                                    }

                                    if (!isFormat) {
                                        keyBoardBuilder.addTextButton("Отмена", 1);
                                        sendTextMessageWithKeyBoard(id, "Укажите в целых часах период напоминаний(по умолчанию стоит 1 час)", keyBoardBuilder.getKeyboardMarkup());
                                    } else {
                                        sendTextMessageWithKeyBoard(id, "Зайди в настройки чтобы настроить уведомления", myMenus.mainMenu(id));
                                    }
                                }
                                break;
                            case 2:
                                if (text.equals("Отмена")) {
                                    sendTextMessageWithKeyBoard(id, "Зайди в настройки чтобы настроить уведомления", myMenus.mainMenu(id));
                                }
                                if (text.equals("Возобновить")) {
                                    db.setPause(id, 0);
                                    sendTextMessageWithKeyBoard(id, "Зайди в настройки чтобы настроить уведомления", myMenus.mainMenu(id));
                                }else if(text.equals("Пауза")){
                                    db.setPause(id, 100000);
                                    sendTextMessageWithKeyBoard(id, "Зайди в настройки чтобы настроить уведомления", myMenus.mainMenu(id));
                                } else {
                                    try {
                                        db.setPause(id, Integer.parseInt(text));
                                        db.setMenuState(id, -1);
                                        keyBoardBuilder.addTextButton("Настройки", 1);
                                        keyBoardBuilder.addTextButton("Пауза", 1);
                                        keyBoardBuilder.addTextButton("Статистика дел", 2);
                                        sendTextMessageWithKeyBoard(id, "Зайди в настройки чтобы настроить уведомления", keyBoardBuilder.getKeyboardMarkup());

                                    } catch (NumberFormatException nfe) {
                                        sendTextMessage(id, "Введите целое число");
                                    }

                                }
                                break;
                        }
                    }
                }
            } else {
                Integer id = update.getCallbackQuery().getFrom().getId();
                String callBackText = update.getCallbackQuery().getData();
                if (callBackText.contains("done:")) {
                    callBackText = callBackText.split(":")[1];
                    db.solveCase(id, Integer.parseInt(callBackText));
                }
            }
        }
    }

    private int checkIsCase(String text) {
        int n = 0;
        text = text.replace(" ", "");
        if (!text.contains("!")) {
            return n;
        } else {
            /*for (int i = 1; i < 5; i++) {
                if (text.substring(i - 1, i).equals("-")) {
                    n++;
                } else break;
            }*/
            for (int i = 0; i < 5; i++) {
                if (text.charAt(i) == '!') {
                    n++;
                } else break;
            }
        }
        return n;
    }


    String getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return getModifyUnixTime(sdf.format(cal.getTime()),3);
    }


    String getModifyTime(String time, int d) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            Date t = sdf.parse(time.split(" ")[0]);
            time = sdf.format(DateUtils.addDays(t, d));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }String getModifyUnixTime(String time, int d) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            Date t = sdf.parse(time);
            time = sdf.format(DateUtils.addHours(t, d));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }

    @Override
    public void onClosing() {

    }
}
