package wdyd;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

public class TgKeyBoardBuilder {

    private ReplyKeyboardMarkup keyboardMarkup;
    private InlineKeyboardMarkup inlineKeyboardMarkup;
    private List<KeyboardRow> keyboard;
    private List<InlineKeyboardButton> inlineRow;
    private List<List<InlineKeyboardButton>> inlineKeyboard;
    private KeyboardRow currentRow;
    private int curLine = 1;
    private int incurLine = 1;

    TgKeyBoardBuilder() {
        keyboardMarkup = new ReplyKeyboardMarkup();
        inlineRow = new ArrayList<>();
        inlineKeyboard = new ArrayList<>();
        inlineKeyboardMarkup = new InlineKeyboardMarkup();
        keyboard = new ArrayList<KeyboardRow>();
        currentRow = new KeyboardRow();
    }

    public ReplyKeyboardMarkup getKeyboardMarkup() {
        keyboard.add(currentRow);
        keyboardMarkup.setKeyboard(keyboard);
        return keyboardMarkup;
    }

    public InlineKeyboardMarkup getInlineKeyboardMarkup() {
        inlineKeyboard.add(inlineRow);
        inlineKeyboardMarkup.setKeyboard(inlineKeyboard);
        return inlineKeyboardMarkup;
    }

    void test() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        InlineKeyboardButton inlineKeyboardButton1 = new InlineKeyboardButton();
        InlineKeyboardButton inlineKeyboardButton2 = new InlineKeyboardButton();

        inlineKeyboardButton1.setText("Тык");
        inlineKeyboardButton1.setCallbackData("Button \"Тык\" has been pressed");
        inlineKeyboardButton2.setText("Тык2");
        inlineKeyboardButton2.setCallbackData("Button \"Тык2\" has been pressed");

        List<InlineKeyboardButton> keyboardButtonsRow1 = new ArrayList<>();
        List<InlineKeyboardButton> keyboardButtonsRow2 = new ArrayList<>();

        keyboardButtonsRow1.add(inlineKeyboardButton1);
        keyboardButtonsRow1.add(new InlineKeyboardButton().setText("Fi4a").setCallbackData("CallFi4a"));
        keyboardButtonsRow2.add(inlineKeyboardButton2);
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        rowList.add(keyboardButtonsRow1);
        rowList.add(keyboardButtonsRow2);
        inlineKeyboardMarkup.setKeyboard(rowList);
        //return new SendMessage().setChatId(chatId).setText("Пример").setReplyMarkup(inlineKeyboardMarkup);
    }

    public void addTextButton(String text, int line) {
        if (line == curLine) {
            currentRow.add(text);
        } else if (line == curLine + 1) {
            curLine = line;
            keyboard.add(currentRow);
            currentRow = new KeyboardRow();
            currentRow.add(text);
        }
    }

    public void addInlineButton(String text, String callbackData, int line) {
        if (line == incurLine) {
            inlineRow.add(new InlineKeyboardButton().setText(text).setCallbackData(callbackData));
        } else if (line == incurLine + 1) {
            incurLine = line;
            inlineKeyboard.add(inlineRow);
            inlineRow = new ArrayList<>();
            inlineRow.add(new InlineKeyboardButton().setText(text).setCallbackData(callbackData));
        }
    }

    public void sendCustomKeyboard(Long chatId) {
        SendMessage message = new SendMessage();
        message.setChatId(chatId);
        message.setText("Custom message text");

        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow row = new KeyboardRow();
        row.add("Row 1 Button 1");
        keyboard.add(row);
        row = new KeyboardRow();
        row.add("Row 2 Button 1");
        keyboard.add(row);
        keyboardMarkup.setKeyboard(keyboard);

    }

}
