package wdyd;

import java.sql.*;
import java.util.ArrayList;


public class DataBase {
    // JDBC URL, username and password of MySQL server
    private final String url = "jdbc:mysql://localhost:3306/";
    private final String user = "root";
    private final String password = "dBM6rW_c";
    //private final String password = "12345678";

    // JDBC variables for opening and managing connection
    private Connection con;
    private Statement stmt;
    private static DataBase db;
    public ResultSet resSet;

    void ConnectionToDB() {
        try {
            //  con = DriverManager.getConnection(url, user, password);
            //stmt = con.createStatement();
            con = null;
            try {
                Class.forName("org.sqlite.JDBC");
                con = DriverManager.getConnection("jdbc:sqlite:trackhoursbot.db");
                stmt = con.createStatement();
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }


            //stmt.execute("CREATE DATABASE IF NOT EXISTS StaffDataBase;");
            //tmt.execute("USE StaffDataBase;");
            stmt.execute("CREATE TABLE IF NOT EXISTS Users(user_id BIGINT primary key NOT NULL,userName TEXT,userNick TEXT,start TIME DEFAULT '10:00',end TIME DEFAULT '22:00',pause INTEGER DEFAULT 0,language INTEGER DEFAULT 0,period INTEGER DEFAULT 1,menu_state INT NOT NULL DEFAULT -1,isActive BOOLEAN);");
            //stmt.execute("CREATE TABLE IF NOT EXISTS Settings(user_id BIGINT primary key NOT NULL,workHourly TEXT,pause INTEGER,isActive BOOLEAN);");
            stmt.execute("CREATE TABLE IF NOT EXISTS History(user_id BIGINT,event_date DATE,event_time TIME,value TEXT);");
            stmt.execute("CREATE TABLE IF NOT EXISTS Cases(id BIGINT,user_id BIGINT,event_date DATE,value TEXT,isDone INTEGER DEFAULT 0);");
            System.out.println("Database connected success");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Database connection error");
        }
    }

    static DataBase getInstanse() {
        if (db == null) {
            db = new DataBase();
        }
        return db;
    }

    DataBase() {
        ConnectionToDB();
    }

    boolean isUserExist(Long user_id) {
        String queryGetId = "SELECT * FROM Users WHERE user_id=" + user_id + " ;";
        try {
            resSet = stmt.executeQuery(queryGetId);
            while (resSet.next()) {
                if (resSet.getInt("user_id") == user_id) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    boolean addUser(Long user_id, String nick) {
        String query = "INSERT INTO Users (user_id,userNick) VALUES (" + user_id + ",'" + nick + "'); ";
        try {
            stmt.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    void closeConnection() {
        try {
            con.close();
        } catch (Exception se) { /*can't do anything */ }
        try {
            if (stmt != null)
                stmt.close();
        } catch (SQLException se) { /*can't do anything */ }

    }

    public void setMenuState(Long from_id, int menu) {
        try {
            stmt.execute("UPDATE Users SET menu_state=" + menu + " WHERE user_id=" + from_id + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    int getMenu(Long from_id) {
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users WHERE user_id=" + from_id + ";");
            while (resSet.next()) {
                return resSet.getInt("menu_state");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void setRank(Long from_id, int rank) {
        try {
            stmt.execute("UPDATE Users SET user_rank=" + rank + " WHERE user_id=" + from_id + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeUser(Long from_id) {
        try {
            stmt.execute("DELETE FROM Users WHERE user_id=" + from_id + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void addEvent(Long user_id, String currentTime, String i) {

        String query = "INSERT INTO History (user_id,event_date,event_time,value) VALUES (" + user_id + ",'" + currentTime.split(" ")[0] + "','" + currentTime.split(" ")[1] + "','" + i + "'); ";
        try {
            stmt.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addCase(Long user_id, String currentTime, String i) {


        try {
            resSet = stmt.executeQuery("SELECT COUNT(*) AS total FROM Cases WHERE user_id=" + user_id + ";");
            String query = "INSERT INTO Cases (id,user_id,event_date,value) VALUES (" + 0 + "," + user_id + ",'" + currentTime.split(" ")[0] + "','" + i + "'); ";
            while (resSet.next()) {
                query = "INSERT INTO Cases (id,user_id,event_date,value) VALUES (" + (resSet.getInt("total") + 1) + "," + user_id + ",'" + currentTime.split(" ")[0] + "','" + i + "'); ";
            }
            stmt.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public ArrayList<Long> getUsersID() {
        ArrayList<Long> result = new ArrayList<Long>();
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users;");

            while (resSet.next()) {
                result.add(resSet.getLong("user_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;

    }

    public int getLang(Long id) {
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users WHERE user_id=" + id + ";");
            while (resSet.next()) {
                return resSet.getInt("language");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void setLang(long user_id, int lang) {
        try {
            stmt.execute("UPDATE Users SET language=" + lang + " WHERE user_id=" + user_id + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isTimeRange(Long user, String time) {
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users WHERE user_id=" + user + " AND start<='" + time + "' AND end>='" + time + "';");
            while (resSet.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isPause(Long user) {
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users WHERE user_id=" + user + ";");
            while (resSet.next()) {
                int pause = resSet.getInt("pause");
                if (pause > 0) {
                    pause--;
                    stmt.execute("UPDATE Users SET pause=" + pause + " WHERE user_id=" + user + ";");

                    return true;
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void setPause(Long id, int pause) {
        try {
            stmt.execute("UPDATE Users SET pause=" + pause + " WHERE user_id=" + id + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setTime(Long id, String s, String e) {
        try {
            stmt.execute("UPDATE Users SET start='" + s + "' WHERE user_id=" + id + ";");
            stmt.execute("UPDATE Users SET end='" + e + "' WHERE user_id=" + id + ";");
        } catch (SQLException eq) {
            eq.printStackTrace();
        }
    }

    String getTime(Long from_id) {
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users WHERE user_id=" + from_id + ";");
            while (resSet.next()) {
                return resSet.getString("start") + "-" + resSet.getString("end");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public ArrayList<Event> getEvents(long id) {
        ArrayList<Event> result = new ArrayList<Event>();
        try {
            resSet = stmt.executeQuery("SELECT * FROM History WHERE user_id = " + id + ";");

            while (resSet.next()) {
                result.add(new Event(resSet.getString("event_date"), resSet.getString("event_time"), resSet.getString("value")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


    public void setPeriod(Long from_id, int per) {
        try {
            stmt.execute("UPDATE Users SET period=" + per + " WHERE user_id=" + from_id + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    int getPeriod(Long from_id) {
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users WHERE user_id=" + from_id + ";");
            while (resSet.next()) {
                return resSet.getInt("period");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void setPeriodValue(Long from_id, int per) {
        try {
            stmt.execute("UPDATE Users SET period_value=" + per + " WHERE user_id=" + from_id + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    int getPeriodValue(Long from_id) {
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users WHERE user_id=" + from_id + ";");
            while (resSet.next()) {
                return resSet.getInt("period_value");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean isStart(Long user, String currentTime) {
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users WHERE user_id=" + user + ";");
            while (resSet.next()) {
                if (resSet.getString("start").equals(currentTime)) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isEnd(Long user, String currentTime) {
        try {
            resSet = stmt.executeQuery("SELECT * FROM Users WHERE user_id=" + user + ";");
            while (resSet.next()) {
                if (resSet.getString("end").equals(currentTime)) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public ArrayList<Case> getCases(long id) {
        ArrayList<Case> result = new ArrayList<Case>();
        try {
            resSet = stmt.executeQuery("SELECT * FROM Cases WHERE user_id = " + id + " AND isDone = 0;");

            while (resSet.next()) {
                result.add(new Case(resSet.getInt("id"), resSet.getString("event_date"), resSet.getString("value")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<Case> getCasesToday(Long user, String currentTime) {
        ArrayList<Case> result = new ArrayList<Case>();
        try {
            resSet = stmt.executeQuery("SELECT * FROM Cases WHERE user_id = " + user + " AND event_date <= '" + currentTime.split(" ")[0] + "' AND isDone = 0;");

            while (resSet.next()) {
                result.add(new Case(resSet.getInt("id"), resSet.getString("event_date"), resSet.getString("value")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<Case> getCasesTomorrow(Long user, String time) {
        ArrayList<Case> result = new ArrayList<Case>();
        try {
            resSet = stmt.executeQuery("SELECT * FROM Cases WHERE user_id = " + user + " AND event_date <= '" + time.split(" ")[0] + "' AND isDone = 0;");

            while (resSet.next()) {
                result.add(new Case(resSet.getInt("id"), resSet.getString("event_date"), resSet.getString("value")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void solveCase(Integer id, int parseInt) {

        try {
            stmt.execute("UPDATE Cases SET isDone=1 WHERE user_id=" + id + " AND id=" + parseInt + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Case> getAllCases(long id) {
            ArrayList<Case> result = new ArrayList<Case>();
            try {
                resSet = stmt.executeQuery("SELECT * FROM Cases WHERE user_id = " + id + ";");

                while (resSet.next()) {
                    result.add(new Case(resSet.getInt("id"), resSet.getString("event_date"), resSet.getString("value"),resSet.getInt("isDone")));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return result;
        }
}
