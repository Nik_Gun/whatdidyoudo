package wdyd;

public class MyStrings {


    static String welcome[] = {"Добро пожаловать!", "Welcome!", "¡Bienvenidos!", "Benvenuto!", "欢迎！"};
    static String[] listOfFile = {"Список файлов", "list of files", "lista de archivos", "elenco dei file", "文件列表"};
    static String enterServerName[] ={"Введите имя сервера что бы по нему в дальнейшем, Вы могли быстро подключиться к нему или введие данные в формате:","Enter the server name that would be on it in the future, you can quickly connect to or enter data in the following format:","Introduzca el nombre del servidor que sería en él en el futuro, puede conectarse rápidamente a o introducir datos en el siguiente formato:","Immettere il nome del server che sarebbe su di esso in futuro, è possibile connettersi rapidamente a o inserire dati nel seguente formato:","进入这将是它在未来的服务器名称，就可以快速连接或输入数据的格式如下："};
    static String enterServerIP[] = {"Введите ip-адрес сервера", "Enter the ip-address of the server", "Introduzca la dirección IP del servidor", "Inserire l'indirizzo IP del server", "输入服务器的IP地址"};
    static String enterServerLogin[] = {"Введите логин от Вашего пользователя", "Enter the username of your user", "Introduzca el nombre de usuario del usuario", "Inserire il nome utente del vostro utente", "输入您的用户的用户名"};
    static String enterServerPassword[] = {"Введите пароль от Вашего пользователя", "Enter the password for your user", "Introduzca la contraseña de su usuario", "Immettere la password per l'utente", "为用户输入密码"};
    static String enterServerType[] = {"Выберете тип подключения", "Select the type of connection", "Seleccionar el tipo de conexión", "Selezionare il tipo di connessione", "选择连接类型"};
    static String[] noServersHave = {"У Вас нет серверов - добавте", "You do not have servers - add the", "No es necesario que los servidores - añadir el", "Non si dispone di server - aggiungere il", "你没有服务器 - 添加"};
    static String[] serverWasCreated = {"Сервер создан", "server set up", "servidor configurado", "del server configurato", "服务器架设"};
    static String[] sorryForForceDisconnected = {"Соединение было прервано так как мы перезагружали наш сервер!", "Connect as we rebooted our server was interrupted!", "Conectar como hemos reiniciado nuestro servidor fue interrumpido!", "Collegare come abbiamo riavviato il nostro server è stata interrotta!", "作为连接我们重新启动我们的服务器被打断！"};
    static String[] fileUpload = {"Файл загружен", "File downloaded", "descargado el archivo", "File scaricati", "文件下载"};
    static String[] fileNotUpload = {"Файл не загружен", "The file is not loaded", "El archivo no se ha cargado", "Il file non viene caricato", "没有加载文件"};
    static String[] enterSharedName = {"Введите никнейм человека с которым хотите поделиться с @ или без нее", "Enter the nickname of the person with whom you want to share with or without @", "Introduzca el apodo de la persona con la que desea compartir con o sin @", "Inserisci il nickname della persona con cui si desidera condividere con o senza @", "输入你想分享的人有或没有的人的昵称@"};
    static String[] sharedUserNotRegist = {"Этот пользователь не пользуется этим ботом:( \n Пожалуйста поделитесь @RemTer_bot", "This user does not use this bot :( \n Please share @RemTer_bot", "Este usuario no utiliza este robot :( \nPor favor comparta @RemTer_bot", "Questo utente non usa questo bot :( \nSi prega di condividere @RemTer_bot", "此用户不使用此漫游:( \n请分享@RemTer_bot"};
    static String[] sharedNewSerrver = {"С вами поделились сервером:", "To share with you the server:", "Para compartir con ustedes el servidor:", "Per condividere con voi il server:", "与您共享服务器："};
    static String[] succsessImport = {"Cервер успешно импортирован!", "The server successfully imported!", "El servidor ha importado correctamente!", "Il server importato con successo!", "服务器成功导入！"};
    static String[] mainMenu = {"Главное меню", "Main menu", "Menú principal", "Menu principale", "主菜单"};
    static String addServer[] = {"Добавить сервер", "Add server", "Agregar servidor", "Aggiungi server", "添加服务器"};
    static String serversList[] = {"Список серверов", "The list of servers", "La lista de servidores", "L'elenco dei server", "服务器的列表"};
    static String connectToServer[] = {"Подключиться", "connect", "conectar", "Collegare", "连接"};
    static String[] disconnect = {"Отключиться", "Logout", "Cerrar sesión", "Disconnettersi", "登出"};
    static String cancel[] = {"Отмена", "cancel", "cancelación", "cancellazione", "消除"};
    static String back[] = {"Назад", "ago", "hace", "fa", "前"};
    static String[] refreash = {"Обновить", "update", "actualizar", "aggiornare", "更新"};
    static String conn[] = {"подкл.", "conn.", "Conn.", "conn.", "康涅狄格州。"};
    static String remove[] = {"удал.", "removed.", "remoto.", "rimosso.", "除去。"};
    static String[] share = {"подел.", "Share.", "Compartir.", "Condividere.", "分享。"};
    static String[] help = {"Помощь", "Help", "Ayuda", "Aiuto", "救命"};
    static String[] export = {"эксп.", "exp.", "exp.", "esp.", "出口"};
    public static String[] enterTheCommand={"Введите команду"};
    public static String[] connecting={"Подключаемся"};
}
