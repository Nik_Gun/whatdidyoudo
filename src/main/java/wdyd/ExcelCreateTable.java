package wdyd;

import com.google.inject.internal.cglib.proxy.$UndeclaredThrowableException;

import java.io.*;
import java.util.ArrayList;

public class ExcelCreateTable {
    private final DataBase db;
    long id = 0;
    ExcelController excelController;

    public ExcelCreateTable(Long to, int template) {
        id = to;
        db = DataBase.getInstanse();
        try {
            excelController = new ExcelController(copyFileUsingStream(new File("excel/templates/template" + template + ".xlsx"), new File("excel/tables/" + id + ".xlsx")));
            if (template == 1) {
                ArrayList<Event> events = db.getEvents(id);
                int row = 1;
                for (Event event : events) {
                    excelController.setTextToCell(event.date.replace("-", ".") + " " + event.time, row, 0);
                    excelController.setTextToCell(event.event, row, 1);
                    row++;
                }
            } else {
                ArrayList<Case> cases = db.getAllCases(id);
                int row = 1;
                for (Case c : cases) {
                    excelController.setTextToCell(c.event_date.replace("-", "."), row, 0);
                    excelController.setTextToCell(c.value, row, 1);
                    if (c.isDone == 1)
                        excelController.setTextToCell("Выполнено", row, 2);
                    else
                        excelController.setTextToCell("Не выполнено", row, 2);
                    row++;
                }
            }
            excelController.closeBook();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String copyFileUsingStream(File source, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
        return dest.getAbsolutePath();
    }
}
